/*
	tcping - main.go

	Copyright (C) 2023 hexaheximal

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"net"
	"os"
	"time"
)

func main() {
	if 2 > len(os.Args) {
		fmt.Println("Usage: tcping server/client")
		os.Exit(1)
	}

	if os.Args[1] == "server" {
		StartServer()
		return
	}

	if os.Args[1] == "client" {
		StartClient()
		os.Exit(1)
		return
	}

	fmt.Println("ERROR: Unknown subcommand")
	os.Exit(1)
}

func StartClient() {
	if 3 > len(os.Args) {
		fmt.Println("Usage: tcping client [address:port]")
		os.Exit(1)
	}

	conn, err := net.Dial("tcp", os.Args[2])

	if err != nil {
		panic(err)
	}

	// ping sequence

	var seq uint64 = 0

	seqBuffer := make([]byte, 8)
	readBuffer := make([]byte, 8)

	for {
		time.Sleep(time.Second)

		seq++

		log.Println("SEQ:", seq)

		if err != nil {
			panic(err)
		}

		binary.BigEndian.PutUint64(seqBuffer, seq)

		n, err := conn.Write(seqBuffer)

		if err != nil {
			log.Println("Failed to send the packet:", err)
			break
		}

		if n != 8 {
			log.Println("WARNING: Expected 8 bytes written, but only", n, "bytes actually got sent Client -> Server")
			continue
		}

		n, err = conn.Read(readBuffer)

		if err != nil {
			log.Println("Failed to read the packet from the server:", err)
			break
		}

		if n != 8 {
			log.Println("WARNING: Expected 8 bytes read, but only", n, "bytes actually got sent Server -> Client")
			continue
		}

		if bytes.Compare(seqBuffer, readBuffer) != 0 {
			log.Println("WARNING: Expected", seqBuffer, "but the server sent", readBuffer)
			continue
		}

		log.Println("OK!")
	}
}

func StartServer() {
	listen, err := net.Listen("tcp", ":1234")

	if err != nil {
		log.Fatalln(err)
	}

	// close listener

	defer listen.Close()

	log.Println("Listening for clients...")

	for {
		conn, err := listen.Accept()

		if err != nil {
			panic(err)
		}

		log.Println("Accepted Connection:", conn.RemoteAddr())
		go HandleConnection(conn)
	}
}

func HandleConnection(conn net.Conn) {
	for {
		buffer := make([]byte, 8)
		n, err := conn.Read(buffer)

		if err != nil {
			conn.Close()
			log.Println("Closed Connection:", conn.RemoteAddr())
			return
		}

		if n != 8 {
			log.Println("WARNING:", conn.RemoteAddr(), "has sent a packet with an invalid size. This shouldn't happen.")
			break
		}

		log.Println(conn.RemoteAddr(), "SEQ:", binary.BigEndian.Uint64(buffer))

		_, err = conn.Write(buffer)

		if err != nil {
			break
		}
	}

	conn.Close()
}
